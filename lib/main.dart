import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:recipe_box/app.dart';
import 'package:recipe_box/recipe_box_bloc_observer.dart';

void main() {
  BlocOverrides.runZoned(
        () => runApp(App()),
    blocObserver: RecipeBoxBlocObserver(),
  );
}