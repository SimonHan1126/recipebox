part of 'recipe_box_bloc.dart';

abstract class RecipeBoxEvent extends Equatable {
  const RecipeBoxEvent();

  @override
  List<Object> get props => [];
}

class InitialDefaultRecipesEvent extends RecipeBoxEvent {}

class LoadSavedRecipesEvent extends RecipeBoxEvent {}

class AddRecipeEvent extends RecipeBoxEvent {
  const AddRecipeEvent(this.newRecipe);

  final Recipe newRecipe;

  @override
  List<Object> get props => [newRecipe];
}

class EditRecipeEvent extends RecipeBoxEvent {
  const EditRecipeEvent(this.changedRecipe, this.index);

  final Recipe changedRecipe;
  final int index;

  @override
  List<Object> get props => [changedRecipe];
}

class RemoveRecipeEvent extends RecipeBoxEvent {
  const RemoveRecipeEvent(this.recipeName);

  final String recipeName;

  @override
  List<Object> get props => [recipeName];
}

class SelectRecipeEvent extends RecipeBoxEvent {
  const SelectRecipeEvent(this.index);

  final int index;

  @override
  List<Object> get props => [index];
}