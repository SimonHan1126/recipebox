part of 'recipe_box_bloc.dart';

abstract class RecipeBoxState extends Equatable {
  const RecipeBoxState();

  @override
  List<Object> get props => [];
}

class RecipesLoading extends RecipeBoxState {}

class RecipesLoaded extends RecipeBoxState {

  const RecipesLoaded(this.recipeList, this.recipes, this.selectedRecipe, this.index);

  final List<Recipe> recipeList;
  final List<String> recipes;
  final Recipe selectedRecipe;
  final int index;

  @override
  List<Object> get props => [recipeList, recipes, selectedRecipe, index];
}

class RecipesLoadingError extends RecipeBoxState {
  const RecipesLoadingError(this.errorMessage);

  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}