import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:recipe_box/recipeBox/models/recipe.dart';
import 'package:recipe_box/recipeBox/repository/shared_preference_repository.dart';

part 'recipe_box_event.dart';
part 'recipe_box_state.dart';

class RecipeBoxBloc extends Bloc<RecipeBoxEvent, RecipeBoxState> {

  final SharedPreferenceRepository sharedPreferenceRepository = new SharedPreferenceRepository();

  RecipeBoxBloc() : super(RecipesLoading()) {
    on<InitialDefaultRecipesEvent>(_initialDefaultRecipes);
    on<AddRecipeEvent>(_newRecipe);
    on<EditRecipeEvent>(_editRecipe);
    on<RemoveRecipeEvent>(_removeRecipe);
    on<SelectRecipeEvent>(_selectRecipe);
  }

  Future<List<Recipe>> _loadLocalRecipes() async {
    return (json.decode(await rootBundle.loadString('assets/recipes.json')) as List).map((e) => Recipe(
        recipe: e['recipe'], ingredients: e['ingredients'].join('\\'), directions: e['directions'].join('\\'))).toList();
  }

  void _loadRecipes(Emitter<RecipeBoxState> emit, List<Recipe> recipeList, Recipe selectedRecipe, int index) {
    List<String> recipes = [];
    for (int i = 0;  i < recipeList.length; i++) {
      recipes.add(recipeList.elementAt(i).recipe);
    }
    emit(RecipesLoaded(recipeList, recipes, selectedRecipe, index));
  }

  void _selectRecipe(SelectRecipeEvent event, Emitter<RecipeBoxState> emit) async {
    emit(RecipesLoading());
    List<Recipe> recipeList = await sharedPreferenceRepository.getRecipeList();
    Recipe selectedRecipe = recipeList.elementAt(event.index);
    _loadRecipes(emit, recipeList, selectedRecipe, event.index);
  }

  void _removeRecipe(RemoveRecipeEvent event, Emitter<RecipeBoxState> emit) async {
    emit(RecipesLoading());
    List<Recipe> recipeList = await sharedPreferenceRepository.getRecipeList();
    for (int i = 0; i < recipeList.length; i++) {
      Recipe recipeObj = recipeList.elementAt(i);
      if (recipeObj.recipe == event.recipeName) {
        recipeList.removeAt(i);
        break;
      }
    }
    await sharedPreferenceRepository.setRecipeList(recipeList);
    Recipe selectedRecipe;
    if (recipeList.length > 0) {
      selectedRecipe = recipeList.elementAt(0);
    } else {
      selectedRecipe = Recipe();
    }
    _loadRecipes(emit, recipeList, selectedRecipe, 0);
  }

  void _editRecipe(EditRecipeEvent event, Emitter<RecipeBoxState> emit) async {
    emit(RecipesLoading());
    List<Recipe> recipeList = await sharedPreferenceRepository.getRecipeList();
    if (event.index >= 0 && event.index < recipeList.length) {
      recipeList[event.index] = event.changedRecipe;
    }
    await sharedPreferenceRepository.setRecipeList(recipeList);
    _loadRecipes(emit, recipeList, event.changedRecipe, event.index);
  }

  void _newRecipe(AddRecipeEvent event, Emitter<RecipeBoxState> emit) async {
    emit(RecipesLoading());
    List<Recipe> recipeList = await sharedPreferenceRepository.getRecipeList();
    bool isFind = false;
    for (int i = 0; i < recipeList.length; i++) {
      Recipe recipeObj = recipeList.elementAt(i);
      if (recipeObj.recipe == event.newRecipe.recipe) {
        isFind = true;
        break;
      }
    }
    if (isFind) {
      // emit(RecipesLoadingError(event.newRecipe.recipe + ' has already been added to he Recipe Box!'));
    } else {
      recipeList.add(event.newRecipe);
      await sharedPreferenceRepository.setRecipeList(recipeList);
    }
    _loadRecipes(emit, recipeList, event.newRecipe, recipeList.length - 1);
  }

  void _initialDefaultRecipes(InitialDefaultRecipesEvent event, Emitter<RecipeBoxState> emit) async {
    emit(RecipesLoading());
    List<Recipe> recipeList;
    bool isFirstRun = await sharedPreferenceRepository.isFirstRun();
    if (isFirstRun) {
      recipeList = await _loadLocalRecipes();
      await sharedPreferenceRepository.setRecipeList(recipeList);
      await sharedPreferenceRepository.saveIsFirstRun();
    } else {
      recipeList = await sharedPreferenceRepository.getRecipeList();
    }
    _loadRecipes(emit, recipeList, recipeList.elementAt(0), 0);
  }
}