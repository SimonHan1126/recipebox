import 'package:equatable/equatable.dart';

class Recipe extends Equatable {
  String recipe;
  String ingredients;
  String directions;

  Recipe({this.recipe = '', this.ingredients = '', this.directions = ''});

  @override
  List<Object?> get props => [recipe, ingredients, directions];

  factory Recipe.fromJson(Map<String, dynamic> json) {
    return Recipe(
        recipe: json['recipe'],
        ingredients: json['ingredients'],
        directions: json['directions']
    );
  }

  Map<String, String> toJson() => {
    'recipe' : recipe,
    'ingredients': ingredients,
    'directions': directions,
  };

  bool isEmpty() {
    return recipe.isEmpty && ingredients.isEmpty && directions.isEmpty;
  }
}
