import 'package:flutter/material.dart';
class CustomText extends StatelessWidget {

  const CustomText(
      this.text,
      this.color,
      this.fontSize,
      this.fontFamily,
      {Key? key}) : super(key: key);

  final String text;
  final Color color;
  final double fontSize;
  final String fontFamily;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontFamily: fontFamily,
          decoration: TextDecoration.none),
    );
  }
}