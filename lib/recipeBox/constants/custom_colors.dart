import 'dart:ui';

class CustomColors {
  CustomColors._();

  static const coalBlue = Color.fromRGBO(8, 76, 97, 1.0);
  static const orange = Color.fromRGBO(244, 96, 54, 1.0);
  static const darkBrown = Color.fromRGBO(72, 29, 36, 1.0);
  static const darkPurple = Color.fromRGBO(97, 97, 150, 1.0);
}