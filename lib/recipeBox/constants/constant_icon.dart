import 'package:flutter/cupertino.dart';

class ConstIcons {
  ConstIcons._();

  static const IconData delete = IconData(0xe1b9, fontFamily: 'MaterialIcons');

  // drive_file_rename_outline_rounded
  static const IconData edit = IconData(0xf6e6, fontFamily: 'MaterialIcons');

  static const IconData add = IconData(0xee3c, fontFamily: 'MaterialIcons');
}