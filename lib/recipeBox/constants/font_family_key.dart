
class FontFamilyKey {
  FontFamilyKey._();

  static const String FUGAZONE = 'FugazOne';
  static const String SONSIEONE = 'SonsieOne';
  static const String MONTSERRAT = 'Montserrat';
}