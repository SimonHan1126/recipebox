import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_box/recipeBox/bloc/recipe_box_bloc.dart';
import 'package:recipe_box/recipeBox/constants/custom_colors.dart';
import 'package:recipe_box/recipeBox/constants/font_family_key.dart';
import 'package:recipe_box/recipeBox/widget/custom_text.dart';

class RecipeList extends StatelessWidget {
  const RecipeList(this.recipes, {Key? key}) : super(key: key);

  final List<String> recipes;

  List<Widget> _recipesWidget(context) {
    List<Widget> widgets = [];
    recipes.asMap().forEach((index, recipe) {
      Color curItemBgColor = index % 2 == 0 ? CustomColors.coalBlue : CustomColors.orange;
      Color curTextColor = index % 2 == 0 ? CustomColors.orange : Colors.white;
      widgets.add(Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
              width: MediaQuery.of(context).size.width - 44,
            color: curItemBgColor,
            child: GestureDetector(onTap: (){
              BlocProvider.of<RecipeBoxBloc>(context).add(SelectRecipeEvent(index));
            }, child: CustomText(recipe, curTextColor, 12, FontFamilyKey.FUGAZONE)),
          ),
        ],
      ));
    });
    return widgets;
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 77,
      width: MediaQuery.of(context).size.width - 30,
      margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
      decoration: BoxDecoration(
        color: CustomColors.coalBlue,
        borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          height: 67,
          margin: EdgeInsets.all(5),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: _recipesWidget(context),
            ),
          ),
        ),
    );
  }
}
