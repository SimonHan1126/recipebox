import 'package:flutter/material.dart';
import 'package:recipe_box/recipeBox/constants/font_family_key.dart';

class RecipeBoxTitle extends StatelessWidget {
  Widget _freeCodeCampIcon() {
    return Container(
      child: Image.asset('assets/free_code_camp.png'),
    );
  }

  Widget _textTitle() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Text(
        'Recipe Box',
        style: TextStyle(
            color: Colors.white, fontSize: 30, fontFamily: FontFamilyKey.SONSIEONE, decoration: TextDecoration.none),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.fromLTRB(0, 50, 0, 10),
      width: MediaQuery.of(context).size.width,
      height: 50,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _freeCodeCampIcon(),
              _textTitle()
            ],
          ),
        ],
      )
    );
  }
}
