import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_box/recipeBox/bloc/recipe_box_bloc.dart';
import 'package:recipe_box/recipeBox/constants/custom_colors.dart';
import 'package:recipe_box/recipeBox/constants/font_family_key.dart';
import 'package:recipe_box/recipeBox/models/recipe.dart';
import 'package:recipe_box/recipeBox/widget/custom_text.dart';

class CustomOverlayDialog extends ModalRoute<void> {

  CustomOverlayDialog(this.recipeObj, this.action, this.currentIndex);

  String action;
  Recipe recipeObj;
  int currentIndex;

  late final TextEditingController recipeNameController = TextEditingController()..text = recipeObj.recipe;
  late final TextEditingController ingredientController = TextEditingController()..text = recipeObj.ingredients;
  late final TextEditingController directionController = TextEditingController()..text = recipeObj.directions;

  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => '';

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _textContainer(title, margin) {
    return Container(
      margin: margin,
      child: CustomText(title, Colors.white, 18, FontFamilyKey.FUGAZONE),
    );
  }

  Widget _textFieldContainer(TextEditingController textEditingController, String key, String hint) {
    return Container(
      margin: EdgeInsets.all(20),
      child: TextField(
        controller: textEditingController,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: hint
        ),
        onChanged: (text) {
          setState(() {
            recipeObj.recipe = recipeNameController.text;
            recipeObj.ingredients = ingredientController.text;
            recipeObj.directions = directionController.text;
          });
        },
      ));
  }

  Widget _buttonContainer(context, buttonText, isCloseBtn) {
    return Container(
      margin: EdgeInsets.all(10),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(primary: CustomColors.coalBlue),
        onPressed: () {
          if (isCloseBtn) {
            Navigator.pop(context);
          } else {
            _inputValidation(context);
          }
        },
        child: CustomText(buttonText, CustomColors.darkBrown, 16, FontFamilyKey.FUGAZONE)
      ),
    );
  }

  Widget _buttons(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buttonContainer(context, action == 'add' ? 'Add' : 'Save', false),
        _buttonContainer(context, 'Close', true),
      ],
    );
  }

  void _inputValidation(context) {
    if (recipeNameController.text.isEmpty) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(""),
            content: new Text("Your recipe must have a name!"),
            actions: <Widget>[
              new ElevatedButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    BlocProvider.of<RecipeBoxBloc>(context).add(action == 'add' ? AddRecipeEvent(recipeObj) : EditRecipeEvent(recipeObj, currentIndex));
    Navigator.of(context).pop();
  }

  Widget _buildOverlayContent(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        decoration: BoxDecoration(
          color: CustomColors.darkPurple,
          borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _textContainer(action == 'add' ? 'Add a Recipe' : 'Edit Recipe', EdgeInsets.fromLTRB(0, 20, 0, 20)),
              _textContainer('Recipe', EdgeInsets.fromLTRB(0, 5, 0, 5)),
              _textFieldContainer(recipeNameController, 'recipe', 'Recipe Name'),
              _textContainer('Ingredients', EdgeInsets.fromLTRB(0, 5, 0, 5)),
              _textFieldContainer(ingredientController, 'ingredients', 'Separate each ingredient with a "\\":Milk \ 2 Eggs \ 1/3 Cup Sugar'),
              _textContainer('Directions', EdgeInsets.fromLTRB(0, 5, 0, 5)),
              _textFieldContainer(directionController, 'directions', 'Separate each step with a "\\":Preheat oven to 350°F \\Combine ingredients in pie crust \\Bake until crust is golden brown. \\'),
              _buttons(context)
            ],
          ),
      ),
    );
  }

  @override
  Widget buildTransitions(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }
}