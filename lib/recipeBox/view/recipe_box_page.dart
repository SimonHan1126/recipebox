import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_box/recipeBox/constants/custom_colors.dart';
import 'package:recipe_box/recipeBox/view/recipe_box_title.dart';
import 'package:recipe_box/recipeBox/view/recipe_detail.dart';
import 'package:recipe_box/recipeBox/view/recipes_list.dart';
import '../bloc/recipe_box_bloc.dart';

class RecipeBoxPage extends StatelessWidget {
  Widget _childWidget(state) {
    if (state is RecipesLoading) {
      return Center(child: CircularProgressIndicator());
    } else if (state is RecipesLoaded) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RecipeBoxTitle(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RecipeList(state.recipes),
            ],
          ),
          RecipeDetail(state.selectedRecipe, state.index)
        ],
      );
    } else if (state is RecipesLoadingError){
      return Text(state.errorMessage);
    } else {
      return Text('Something went wrong');
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeBoxBloc, RecipeBoxState>(
        builder: (context, state) {
      return Container(
        color: CustomColors.orange,
        child: _childWidget(state),
      );
    });
  }
}
