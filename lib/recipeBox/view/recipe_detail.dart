import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_box/recipeBox/bloc/recipe_box_bloc.dart';
import 'package:recipe_box/recipeBox/constants/custom_colors.dart';
import 'package:recipe_box/recipeBox/constants/constant_icon.dart';
import 'package:recipe_box/recipeBox/constants/font_family_key.dart';
import 'package:recipe_box/recipeBox/models/recipe.dart';
import 'package:recipe_box/recipeBox/view/custom_overlay_dialog.dart';
import 'package:recipe_box/recipeBox/widget/custom_text.dart';

class RecipeDetail extends StatelessWidget {

  const RecipeDetail(this.selected, this.currentIndex, {Key? key}) : super(key: key);

  final Recipe selected;
  final int currentIndex;

  Widget _detailTop(context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width - 30,
      margin: EdgeInsets.fromLTRB(8, 0, 5, 0),
      decoration: BoxDecoration(
        color: CustomColors.coalBlue,
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10))),
      child: Row(
        children: <Widget>[
          CustomText(selected.recipe, Colors.white, 23, FontFamilyKey.FUGAZONE),
          SizedBox(width: 5),
          GestureDetector(onTap: (){
            if (!selected.isEmpty()) {
              _removeConfirmAlertDialog(context, selected.recipe);
            }
          }, child: Icon(ConstIcons.delete, color: CustomColors.darkBrown)),
          SizedBox(width: 5),
          GestureDetector(onTap: (){
            if (!selected.isEmpty()) {
              Navigator.of(context).push(CustomOverlayDialog(selected, 'edit', currentIndex));
            }
          }, child: Icon(ConstIcons.edit, color: CustomColors.darkBrown)),
        ],
      )
    );
  }

  void _removeConfirmAlertDialog(context, recipeName) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(""),
          content: new Text("Are you sure you want to delete " + recipeName + " from the Recipe Box?"),
          actions: <Widget>[
            ElevatedButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            ElevatedButton(
              child: Text("OK"),
              onPressed: () {
                BlocProvider.of<RecipeBoxBloc>(context).add(RemoveRecipeEvent(recipeName));
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _detailBodyContentTitle(String text) {
    return Container(
      margin: EdgeInsets.fromLTRB(8, 15, 5, 15),
      child: CustomText(text, Colors.white, 16, FontFamilyKey.FUGAZONE),
    );
  }

  Widget _detailBodyContentListItem(Widget startWith, String itemContent) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(width: 20),
        startWith,
        Expanded(
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
            child: CustomText(itemContent, Colors.white, 12, FontFamilyKey.MONTSERRAT),
          ),
        ),
      ],
    );
  }

  List<Widget> _detailBodyContent() {
    List<Widget> widgetList = [];
    if (selected.ingredients.isNotEmpty) {
      widgetList.add(_detailBodyContentTitle('Ingredients:'));
      selected.ingredients.split('\\').asMap().forEach((key, value) {
        widgetList.add(_detailBodyContentListItem(CustomText("•   ", Colors.white, 12, FontFamilyKey.MONTSERRAT), value));
      });
    }
    if (selected.directions.isNotEmpty) {
      widgetList.add(_detailBodyContentTitle('Directions:'));
      selected.directions.split('\\').asMap().forEach((key, value) {
        widgetList.add(_detailBodyContentListItem(CustomText((key + 1).toString() + ". ", Colors.white, 12, FontFamilyKey.MONTSERRAT), value));
      });
    }
    widgetList.add(SizedBox(height: 20));
    return widgetList;
  }

  Widget _detailBody(context) {
    return Container(
      height: 260,
      width: MediaQuery.of(context).size.width - 30,
      color: CustomColors.darkBrown,
      child: Container(
        height: 250,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _detailBodyContent(),
          ),
        ),
      )
    );
  }

  Widget _detailBottom(context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width - 30,
      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
      decoration: BoxDecoration(
        color: CustomColors.coalBlue,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))),
      child: Row(
        children: <Widget>[
          GestureDetector(onTap: (){
            Navigator.of(context).push(CustomOverlayDialog(Recipe(), 'add', currentIndex));
          }, child: Icon(ConstIcons.add, color: CustomColors.darkBrown))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 360,
      width: MediaQuery.of(context).size.width - 30,
      margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
      decoration: BoxDecoration(
        color: CustomColors.coalBlue,
        borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          height: 67,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _detailTop(context),
                _detailBody(context),
                _detailBottom(context)
              ],
            ),
          ),
        )
    );
  }
}