import 'dart:convert';
import 'package:recipe_box/recipeBox/models/recipe.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceRepository {

  Future<List<Recipe>> getRecipeList() async {
    final prefs = await SharedPreferences.getInstance();
    final String recipeListString = prefs.getString('recipeList') ?? '';
    if (recipeListString == '') {
      return [Recipe()];
    } else {
      return (json.decode(recipeListString) as List).map((e) => Recipe(
          recipe: e['recipe'], ingredients: e['ingredients'], directions: e['directions'])).toList();
    }
  }

  Future<void> setRecipeList(List<Recipe> list) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('recipeList', jsonEncode(list));
  }

  Future<bool> isFirstRun() async {
    final prefs = await SharedPreferences.getInstance();
    final bool recipeListString = prefs.getBool('isFirstRun') ?? true;
    return recipeListString;
  }

  Future<void> saveIsFirstRun() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('isFirstRun', false);
  }
}
