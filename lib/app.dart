import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_box/recipeBox/bloc/recipe_box_bloc.dart';
import 'package:recipe_box/recipeBox/view/recipe_box_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => RecipeBoxBloc()..add(InitialDefaultRecipesEvent()))
      ],
      child: MaterialApp(
        title: 'Recipe Box',
        initialRoute: '/',
        routes: {
          '/': (_) => RecipeBoxPage()
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}